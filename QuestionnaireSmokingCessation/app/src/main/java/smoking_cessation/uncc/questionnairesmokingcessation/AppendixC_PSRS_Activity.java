package smoking_cessation.uncc.questionnairesmokingcessation;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

/**
 * Created by rakeshbalan on 4/21/2016.
 */
public class AppendixC_PSRS_Activity extends AppCompatActivity {
    HashMap<Integer, AppendixCBean> hashMapAppendixC;
    String[] appendixC_Q;
    String[] appendixC_A;
    static int appendixCAnswerCount = -1;
    TextView textViewAppendixCQuestion;
    RadioGroup radioGroupAppendixC;
    RadioButton radioButtonAppendixCA1;
    RadioButton radioButtonAppendixCA2;
    RadioButton radioButtonAppendixCA3;
    ImageButton imageButtonBack;
    ImageButton imageButtonFront;
    Button buttonSubmitAppendixC;
    static int countAppendixC = 1;
    int checkCountAppendixC = 0;
    ProgressDialog pDialogAppendixC;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.appendix_c_layout);

        hashMapAppendixC = new HashMap<Integer, AppendixCBean>();

        appendixC_Q = getResources().getStringArray(R.array.appendixC_Q);
        appendixC_A = getResources().getStringArray(R.array.appendixC_A);

        for (int i = 0; i < appendixC_Q.length; i++)
        {
            hashMapAppendixC.put(i+1, new AppendixCBean(null, appendixC_Q[i], appendixC_A[++appendixCAnswerCount], appendixC_A[++appendixCAnswerCount], appendixC_A[++appendixCAnswerCount]));
        }

//        for (int i = 1; i <= hashMapAppendixC.size(); i++)
//        {
//            Log.d("debug", hashMapAppendixC.get(i).getAppendixBQuestion());
//            Log.d("debug", hashMapAppendixC.get(i).getAppendixBAnswer1());
//            Log.d("debug", hashMapAppendixC.get(i).getAppendixBAnswer2());
//            Log.d("debug", hashMapAppendixC.get(i).getAppendixBAnswer3());
//            Log.d("debug", " ");
//        }

        textViewAppendixCQuestion = (TextView) findViewById(R.id.textViewAppendixCQuestion);
        radioGroupAppendixC = (RadioGroup) findViewById(R.id.radioGroupAppendixC);
        radioButtonAppendixCA1 = (RadioButton) findViewById(R.id.radioButtonAppendixCA1);
        radioButtonAppendixCA2 = (RadioButton) findViewById(R.id.radioButtonAppendixCA2);
        radioButtonAppendixCA3 = (RadioButton) findViewById(R.id.radioButtonAppendixCA3);
        imageButtonBack = (ImageButton) findViewById(R.id.imageButtonBack);
        imageButtonFront = (ImageButton) findViewById(R.id.imageButtonFront);
        buttonSubmitAppendixC = (Button) findViewById(R.id.buttonSubmitAppendixC);


        textViewAppendixCQuestion.setText(hashMapAppendixC.get(countAppendixC).getAppendixCQuestion());
        radioButtonAppendixCA1.setText(hashMapAppendixC.get(countAppendixC).getAppendixCAnswer1());
        radioButtonAppendixCA2.setText(hashMapAppendixC.get(countAppendixC).getAppendixCAnswer2());
        radioButtonAppendixCA3.setText(hashMapAppendixC.get(countAppendixC).getAppendixCAnswer3());

        imageButtonBack.setEnabled(false);
        imageButtonFront.setEnabled(true);

        imageButtonFront.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (radioButtonAppendixCA1.isChecked()) {
                    checkCountAppendixC = 1;
                } else if (radioButtonAppendixCA2.isChecked()) {
                    checkCountAppendixC = 2;
                } else if (radioButtonAppendixCA3.isChecked()) {
                    checkCountAppendixC = 3;
                }

                hashMapAppendixC.get(countAppendixC).setAppendixCResponse(String.valueOf(checkCountAppendixC));

                countAppendixC++;
                if (countAppendixC == 23) {
                    imageButtonFront.setEnabled(false);
                    imageButtonBack.setEnabled(true);
                    buttonSubmitAppendixC.setVisibility(View.VISIBLE);
                } else {
                    imageButtonFront.setEnabled(true);
                    imageButtonBack.setEnabled(true);
                    buttonSubmitAppendixC.setVisibility(View.INVISIBLE);
                }
                textViewAppendixCQuestion.setText(hashMapAppendixC.get(countAppendixC).getAppendixCQuestion());
                radioButtonAppendixCA1.setText(hashMapAppendixC.get(countAppendixC).getAppendixCAnswer1());
                radioButtonAppendixCA2.setText(hashMapAppendixC.get(countAppendixC).getAppendixCAnswer2());
                radioButtonAppendixCA3.setText(hashMapAppendixC.get(countAppendixC).getAppendixCAnswer3());
            }
        });

        imageButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (radioButtonAppendixCA1.isChecked()) {
                    checkCountAppendixC = 1;
                } else if (radioButtonAppendixCA2.isChecked()) {
                    checkCountAppendixC = 2;
                } else if (radioButtonAppendixCA3.isChecked()) {
                    checkCountAppendixC = 3;
                }

                hashMapAppendixC.get(countAppendixC).setAppendixCResponse(String.valueOf(checkCountAppendixC));

                countAppendixC--;
                if (countAppendixC == 1) {
                    imageButtonFront.setEnabled(true);
                    imageButtonBack.setEnabled(false);
                    buttonSubmitAppendixC.setVisibility(View.INVISIBLE);
                } else {
                    imageButtonFront.setEnabled(true);
                    imageButtonBack.setEnabled(true);
                    buttonSubmitAppendixC.setVisibility(View.INVISIBLE);
                }
                textViewAppendixCQuestion.setText(hashMapAppendixC.get(countAppendixC).getAppendixCQuestion());
                radioButtonAppendixCA1.setText(hashMapAppendixC.get(countAppendixC).getAppendixCAnswer1());
                radioButtonAppendixCA2.setText(hashMapAppendixC.get(countAppendixC).getAppendixCAnswer2());
                radioButtonAppendixCA3.setText(hashMapAppendixC.get(countAppendixC).getAppendixCAnswer3());
            }
        });

        buttonSubmitAppendixC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("debug", String.valueOf(countAppendixC));
                if (radioButtonAppendixCA1.isChecked()) {
                    checkCountAppendixC = 1;
                } else if (radioButtonAppendixCA2.isChecked()) {
                    checkCountAppendixC = 2;
                } else if (radioButtonAppendixCA3.isChecked()) {
                    checkCountAppendixC = 3;
                }
                hashMapAppendixC.get(countAppendixC).setAppendixCResponse(String.valueOf(checkCountAppendixC));


                try
                {
                    JSONObject jsonObjectAppendixC = new JSONObject();
                    jsonObjectAppendixC.put("appendixc_q1", hashMapAppendixC.get(1).getAppendixCResponse().toString());
                    jsonObjectAppendixC.put("appendixc_q2", hashMapAppendixC.get(2).getAppendixCResponse().toString());
                    jsonObjectAppendixC.put("appendixc_q3", hashMapAppendixC.get(3).getAppendixCResponse().toString());
                    jsonObjectAppendixC.put("appendixc_q4", hashMapAppendixC.get(4).getAppendixCResponse().toString());
                    jsonObjectAppendixC.put("appendixc_q5", hashMapAppendixC.get(5).getAppendixCResponse().toString());
                    jsonObjectAppendixC.put("appendixc_q6", hashMapAppendixC.get(6).getAppendixCResponse().toString());
                    jsonObjectAppendixC.put("appendixc_q7", hashMapAppendixC.get(7).getAppendixCResponse().toString());
                    jsonObjectAppendixC.put("appendixc_q8", hashMapAppendixC.get(8).getAppendixCResponse().toString());
                    jsonObjectAppendixC.put("appendixc_q9", hashMapAppendixC.get(9).getAppendixCResponse().toString());
                    jsonObjectAppendixC.put("appendixc_q10", hashMapAppendixC.get(10).getAppendixCResponse().toString());
                    jsonObjectAppendixC.put("appendixc_q11", hashMapAppendixC.get(11).getAppendixCResponse().toString());
                    jsonObjectAppendixC.put("appendixc_q12", hashMapAppendixC.get(12).getAppendixCResponse().toString());
                    jsonObjectAppendixC.put("appendixc_q13", hashMapAppendixC.get(13).getAppendixCResponse().toString());
                    jsonObjectAppendixC.put("appendixc_q14", hashMapAppendixC.get(14).getAppendixCResponse().toString());
                    jsonObjectAppendixC.put("appendixc_q15", hashMapAppendixC.get(15).getAppendixCResponse().toString());
                    jsonObjectAppendixC.put("appendixc_q16", hashMapAppendixC.get(16).getAppendixCResponse().toString());
                    jsonObjectAppendixC.put("appendixc_q17", hashMapAppendixC.get(17).getAppendixCResponse().toString());
                    jsonObjectAppendixC.put("appendixc_q18", hashMapAppendixC.get(18).getAppendixCResponse().toString());
                    jsonObjectAppendixC.put("appendixc_q19", hashMapAppendixC.get(19).getAppendixCResponse().toString());
                    jsonObjectAppendixC.put("appendixc_q20", hashMapAppendixC.get(20).getAppendixCResponse().toString());
                    jsonObjectAppendixC.put("appendixc_q21", hashMapAppendixC.get(21).getAppendixCResponse().toString());
                    jsonObjectAppendixC.put("appendixc_q22", hashMapAppendixC.get(22).getAppendixCResponse().toString());
                    jsonObjectAppendixC.put("appendixc_q23", hashMapAppendixC.get(23).getAppendixCResponse().toString());


                    Log.d("debug", jsonObjectAppendixC.toString());
                    if(isConnectedOnline())
                    {
                        new sendAppendixCJsonDataToAWSLinux(jsonObjectAppendixC).execute("http://ec2-52-90-253-42.compute-1.amazonaws.com/appendix-c.php");
                    }
                    else
                    {
                        Toast.makeText(AppendixC_PSRS_Activity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }

                }catch(JSONException e)
                {
                    e.printStackTrace();
                }


            }
        });
    }

    public class sendAppendixCJsonDataToAWSLinux extends AsyncTask<String, Void, String>
    {
        JSONObject jsonObjectAppendixC;

        public sendAppendixCJsonDataToAWSLinux(JSONObject jsonObjectAppendixC)
        {
            this.jsonObjectAppendixC = jsonObjectAppendixC;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialogAppendixC = new ProgressDialog(AppendixC_PSRS_Activity.this);
            pDialogAppendixC.setCancelable(true);
            pDialogAppendixC.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pDialogAppendixC.setTitle("Loading...");
            pDialogAppendixC.setMessage("Saving in Database");
            pDialogAppendixC.show();
        }

        @Override
        protected void onPostExecute(String returnVariable) {
            super.onPostExecute(returnVariable);
            pDialogAppendixC.dismiss();
            Toast.makeText(AppendixC_PSRS_Activity.this, returnVariable, Toast.LENGTH_SHORT).show();
        }

        @Override
        protected String doInBackground(String... params) {
            String returnString = null;
            try
            {
                URL url = new URL(params[0]);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("POST");
                con.setDoOutput(true);
                con.setDoInput(true);
                con.setRequestProperty("Content-Type", "application/json");
                con.connect();

                OutputStreamWriter out = new OutputStreamWriter(con.getOutputStream());
                out.write(jsonObjectAppendixC.toString());
                out.close();

                //Log.d("debug", jsonObjectAppendixB.toString());


                BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
                returnString = br.readLine().toString();
                Log.d("debug", returnString);
                con.disconnect();
                br.close();


            }
            catch (IOException e)
            {
                e.printStackTrace();
            }

            return returnString;
        }
    }

    private boolean isConnectedOnline(){
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if(networkInfo != null && networkInfo.isConnected()){
            return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
