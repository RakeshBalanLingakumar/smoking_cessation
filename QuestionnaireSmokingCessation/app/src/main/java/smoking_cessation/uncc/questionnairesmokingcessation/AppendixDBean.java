package smoking_cessation.uncc.questionnairesmokingcessation;

/**
 * Created by rakeshbalan on 4/21/2016.
 */
public class AppendixDBean {
    private String appendixDResponse;
    private String appendixDQuestion;

    public AppendixDBean(String appendixDResponse, String appendixDQuestion) {
        this.appendixDResponse = appendixDResponse;
        this.appendixDQuestion = appendixDQuestion;
    }

    public String getAppendixDResponse() {
        return appendixDResponse;
    }

    public void setAppendixDResponse(String appendixDResponse) {
        this.appendixDResponse = appendixDResponse;
    }

    public String getAppendixDQuestion() {
        return appendixDQuestion;
    }

    public void setAppendixDQuestion(String appendixDQuestion) {
        this.appendixDQuestion = appendixDQuestion;
    }
}
