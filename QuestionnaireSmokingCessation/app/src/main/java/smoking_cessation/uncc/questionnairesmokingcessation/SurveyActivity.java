package smoking_cessation.uncc.questionnairesmokingcessation;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

/**
 * Created by rakeshbalan on 4/21/2016.
 */
public class SurveyActivity extends AppCompatActivity{
    Button buttonAppendixB;
    Button buttonAppendixC;
    Button buttonAppendixD;
    Button buttonAppendixE;
    Button buttonAppendixF;
    Button buttonAppendixG;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.temp_all_survey);


        buttonAppendixB = (Button) findViewById(R.id.buttonAppendixB);
        buttonAppendixC = (Button) findViewById(R.id.buttonAppendixC);
        buttonAppendixD = (Button) findViewById(R.id.buttonAppendixD);
        buttonAppendixE = (Button) findViewById(R.id.buttonAppendixE);
        buttonAppendixF = (Button) findViewById(R.id.buttonAppendixF);
        buttonAppendixG = (Button) findViewById(R.id.buttonAppendixG);


        buttonAppendixB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SurveyActivity.this, AppendixB_PSS_Activity.class));
            }
        });

        buttonAppendixC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SurveyActivity.this, AppendixC_PSRS_Activity.class));
            }
        });


        buttonAppendixD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SurveyActivity.this, AppendixD_PANAS_Activity.class));
            }
        });

        buttonAppendixE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SurveyActivity.this, AppendixE_ERQ_Activity.class));
            }
        });

        buttonAppendixF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SurveyActivity.this, AppendixF_CESD_Activity.class));
            }
        });

        buttonAppendixG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SurveyActivity.this, AppendixG_unknown.class));
            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
