package smoking_cessation.uncc.questionnairesmokingcessation;

/**
 * Created by rakeshbalan on 4/21/2016.
 */
public class AppendixCBean {
    private String appendixCResponse;
    private String appendixCQuestion;
    private String appendixCAnswer1;
    private String appendixCAnswer2;
    private String appendixCAnswer3;

    public AppendixCBean(String appendixCResponse, String appendixCQuestion, String appendixCAnswer1, String appendixCAnswer2, String appendixCAnswer3) {
        this.appendixCResponse = appendixCResponse;
        this.appendixCQuestion = appendixCQuestion;
        this.appendixCAnswer1 = appendixCAnswer1;
        this.appendixCAnswer2 = appendixCAnswer2;
        this.appendixCAnswer3 = appendixCAnswer3;
    }

    public String getAppendixCResponse() {
        return appendixCResponse;
    }

    public void setAppendixCResponse(String appendixCResponse) {
        this.appendixCResponse = appendixCResponse;
    }

    public String getAppendixCQuestion() {
        return appendixCQuestion;
    }

    public void setAppendixCQuestion(String appendixCQuestion) {
        this.appendixCQuestion = appendixCQuestion;
    }

    public String getAppendixCAnswer1() {
        return appendixCAnswer1;
    }

    public void setAppendixCAnswer1(String appendixCAnswer1) {
        this.appendixCAnswer1 = appendixCAnswer1;
    }

    public String getAppendixCAnswer2() {
        return appendixCAnswer2;
    }

    public void setAppendixCAnswer2(String appendixCAnswer2) {
        this.appendixCAnswer2 = appendixCAnswer2;
    }

    public String getAppendixCAnswer3() {
        return appendixCAnswer3;
    }

    public void setAppendixCAnswer3(String appendixCAnswer3) {
        this.appendixCAnswer3 = appendixCAnswer3;
    }
}
