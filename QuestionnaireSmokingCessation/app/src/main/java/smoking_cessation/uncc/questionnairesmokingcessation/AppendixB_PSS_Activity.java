package smoking_cessation.uncc.questionnairesmokingcessation;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

/**
 * Created by rakeshbalan on 4/21/2016.
 */
public class AppendixB_PSS_Activity extends AppCompatActivity{
    HashMap<Integer, AppendixBBean> hashMapAppendixB;
    String[] appendixB_Q;
    TextView textViewAppendixBQuestion;
    RadioGroup radioGroupAppendixB;
    ImageButton imageButtonBack;
    ImageButton imageButtonFront;
    static int countAppendixB = 1;
    Button buttonSubmitAppendixB;
    ProgressDialog pDialogAppendixB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.appendix_b_layout);

        hashMapAppendixB = new HashMap<Integer, AppendixBBean>();

        appendixB_Q = getResources().getStringArray(R.array.appendixB_Q);

        for (int i=0; i< appendixB_Q.length; i++)
        {
            hashMapAppendixB.put(i + 1, new AppendixBBean(null, appendixB_Q[i]));
        }

        textViewAppendixBQuestion = (TextView) findViewById(R.id.textViewAppendixBQuestion);
        radioGroupAppendixB = (RadioGroup) findViewById(R.id.radioGroupAppendixB);
        imageButtonBack = (ImageButton) findViewById(R.id.imageButtonBack);
        imageButtonFront = (ImageButton) findViewById(R.id.imageButtonFront);
        buttonSubmitAppendixB = (Button) findViewById(R.id.buttonSubmitAppendixB);

        textViewAppendixBQuestion.setText(hashMapAppendixB.get(countAppendixB).getAppendixBQuestion());
        imageButtonBack.setEnabled(false);
        imageButtonFront.setEnabled(true);

        imageButtonFront.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                hashMapAppendixB.get(countAppendixB).setAppendixBResponse(((RadioButton) findViewById(radioGroupAppendixB.getCheckedRadioButtonId())).getText().toString());

                countAppendixB++;
                if (countAppendixB == 10) {
                    imageButtonFront.setEnabled(false);
                    imageButtonBack.setEnabled(true);
                    buttonSubmitAppendixB.setVisibility(View.VISIBLE);
                } else {
                    imageButtonFront.setEnabled(true);
                    imageButtonBack.setEnabled(true);
                    buttonSubmitAppendixB.setVisibility(View.INVISIBLE);
                }
                textViewAppendixBQuestion.setText(hashMapAppendixB.get(countAppendixB).getAppendixBQuestion());
            }
        });

        imageButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                hashMapAppendixB.get(countAppendixB).setAppendixBResponse(((RadioButton) findViewById(radioGroupAppendixB.getCheckedRadioButtonId())).getText().toString());

                countAppendixB--;
                if (countAppendixB == 1)
                {
                    imageButtonFront.setEnabled(true);
                    imageButtonBack.setEnabled(false);
                    buttonSubmitAppendixB.setVisibility(View.INVISIBLE);
                }
                else
                {
                    imageButtonFront.setEnabled(true);
                    imageButtonBack.setEnabled(true);
                    buttonSubmitAppendixB.setVisibility(View.INVISIBLE);
                }
                textViewAppendixBQuestion.setText(hashMapAppendixB.get(countAppendixB).getAppendixBQuestion());

            }
        });


        buttonSubmitAppendixB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try
                {
                    Log.d("debug", String.valueOf(countAppendixB));
                    hashMapAppendixB.get(countAppendixB).setAppendixBResponse(((RadioButton) findViewById(radioGroupAppendixB.getCheckedRadioButtonId())).getText().toString());

                    JSONObject jsonObjectAppendixB = new JSONObject();
                    jsonObjectAppendixB.put("appendixb_q1", hashMapAppendixB.get(1).getAppendixBResponse().toString() );
                    jsonObjectAppendixB.put("appendixb_q2", hashMapAppendixB.get(2).getAppendixBResponse().toString() );
                    jsonObjectAppendixB.put("appendixb_q3", hashMapAppendixB.get(3).getAppendixBResponse().toString() );
                    jsonObjectAppendixB.put("appendixb_q4", hashMapAppendixB.get(4).getAppendixBResponse().toString() );
                    jsonObjectAppendixB.put("appendixb_q5", hashMapAppendixB.get(5).getAppendixBResponse().toString() );
                    jsonObjectAppendixB.put("appendixb_q6", hashMapAppendixB.get(6).getAppendixBResponse().toString() );
                    jsonObjectAppendixB.put("appendixb_q7", hashMapAppendixB.get(7).getAppendixBResponse().toString() );
                    jsonObjectAppendixB.put("appendixb_q8", hashMapAppendixB.get(8).getAppendixBResponse().toString() );
                    jsonObjectAppendixB.put("appendixb_q9", hashMapAppendixB.get(9).getAppendixBResponse().toString() );
                    jsonObjectAppendixB.put("appendixb_q10", hashMapAppendixB.get(10).getAppendixBResponse().toString());


                    Log.d("debug", jsonObjectAppendixB.toString());
                    if(isConnectedOnline())
                    {
                        new sendAppendixBJsonDataToAWSLinux(jsonObjectAppendixB).execute("http://ec2-52-90-253-42.compute-1.amazonaws.com/appendix-b.php");
                    }
                    else
                    {
                        Toast.makeText(AppendixB_PSS_Activity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }

                }catch(JSONException e)
                {
                    e.printStackTrace();
                }

            }
        });
    }

    public class sendAppendixBJsonDataToAWSLinux extends AsyncTask<String, Void, String>
    {
        JSONObject jsonObjectAppendixB;

        public sendAppendixBJsonDataToAWSLinux(JSONObject jsonObjectAppendixB)
        {
            this.jsonObjectAppendixB = jsonObjectAppendixB;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialogAppendixB = new ProgressDialog(AppendixB_PSS_Activity.this);
            pDialogAppendixB.setCancelable(true);
            pDialogAppendixB.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pDialogAppendixB.setTitle("Loading...");
            pDialogAppendixB.setMessage("Saving in Database");
            pDialogAppendixB.show();
        }



        @Override
        protected String doInBackground(String... params) {
            String returnString = null;
            try
            {
                URL url = new URL(params[0]);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("POST");
                con.setDoOutput(true);
                con.setDoInput(true);
                con.setRequestProperty("Content-Type", "application/json");
                con.connect();

                OutputStreamWriter out = new OutputStreamWriter(con.getOutputStream());
                out.write(jsonObjectAppendixB.toString());
                out.close();

                //Log.d("debug", jsonObjectAppendixB.toString());


                BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
                returnString = br.readLine().toString();
                Log.d("debug", returnString);
                con.disconnect();
                br.close();


            }
            catch (IOException e)
            {
                e.printStackTrace();
            }

            return returnString;
        }


        @Override
        protected void onPostExecute(String returnVariable) {
            super.onPostExecute(returnVariable);
            pDialogAppendixB.dismiss();
            Toast.makeText(AppendixB_PSS_Activity.this, returnVariable, Toast.LENGTH_SHORT).show();
        }
    }

    private boolean isConnectedOnline(){
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if(networkInfo != null && networkInfo.isConnected()){
            return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
