package smoking_cessation.uncc.questionnairesmokingcessation;

/**
 * Created by rakeshbalan on 4/21/2016.
 */
public class AppendixBBean {
    private String appendixBResponse;
    private String appendixBQuestion;

    public AppendixBBean(String appendixBResponse, String appendixBQuestion) {
        this.appendixBResponse = appendixBResponse;
        this.appendixBQuestion = appendixBQuestion;
    }

    public void setAppendixBResponse(String appendixBResponse) {
        this.appendixBResponse = appendixBResponse;
    }

    public String getAppendixBResponse() {
        return appendixBResponse;
    }

    public String getAppendixBQuestion() {
        return appendixBQuestion;
    }
}
