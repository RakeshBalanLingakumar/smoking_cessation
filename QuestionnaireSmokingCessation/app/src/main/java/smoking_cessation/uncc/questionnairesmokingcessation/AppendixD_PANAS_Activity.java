package smoking_cessation.uncc.questionnairesmokingcessation;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

/**
 * Created by rakeshbalan on 4/21/2016.
 */
public class AppendixD_PANAS_Activity extends AppCompatActivity {
    HashMap<Integer, AppendixDBean> hashMapAppendixD;
    String[] appendixD_Q;

    TextView textViewAppendixDQuestion;
    Spinner spinnerAppendixD;
    ImageButton imageButtonBack;
    ImageButton imageButtonFront;
    Button buttonSubmitAppendixD;
    static int countAppendixD = 1;
    ProgressDialog pDialogAppendixD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.appendix_d_layout);

        hashMapAppendixD = new HashMap<Integer, AppendixDBean>();

        appendixD_Q = getResources().getStringArray(R.array.appendixD_Q);

        for (int i = 0; i < appendixD_Q.length; i++)
        {
            hashMapAppendixD.put(i+1, new AppendixDBean(null, appendixD_Q[i]));
        }

        textViewAppendixDQuestion = (TextView) findViewById(R.id.textViewAppendixDQuestion);
        spinnerAppendixD = (Spinner) findViewById(R.id.spinnerAppendixD);
        imageButtonBack = (ImageButton) findViewById(R.id.imageButtonBack);
        imageButtonFront = (ImageButton) findViewById(R.id.imageButtonFront);
        buttonSubmitAppendixD = (Button) findViewById(R.id.buttonSubmitAppendixD);


        textViewAppendixDQuestion.setText(hashMapAppendixD.get(countAppendixD).getAppendixDQuestion());
        imageButtonBack.setEnabled(false);
        imageButtonFront.setEnabled(true);



        imageButtonFront.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                hashMapAppendixD.get(countAppendixD).setAppendixDResponse(spinnerAppendixD.getSelectedItem().toString());

                countAppendixD++;
                if (countAppendixD == 20) {
                    imageButtonFront.setEnabled(false);
                    imageButtonBack.setEnabled(true);
                    buttonSubmitAppendixD.setVisibility(View.VISIBLE);
                } else {
                    imageButtonFront.setEnabled(true);
                    imageButtonBack.setEnabled(true);
                    buttonSubmitAppendixD.setVisibility(View.INVISIBLE);
                }
                textViewAppendixDQuestion.setText(hashMapAppendixD.get(countAppendixD).getAppendixDQuestion());

            }
        });

        imageButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                hashMapAppendixD.get(countAppendixD).setAppendixDResponse(spinnerAppendixD.getSelectedItem().toString());

                countAppendixD--;
                if (countAppendixD == 1) {
                    imageButtonFront.setEnabled(true);
                    imageButtonBack.setEnabled(false);
                    buttonSubmitAppendixD.setVisibility(View.INVISIBLE);
                } else {
                    imageButtonFront.setEnabled(true);
                    imageButtonBack.setEnabled(true);
                    buttonSubmitAppendixD.setVisibility(View.INVISIBLE);
                }
                textViewAppendixDQuestion.setText(hashMapAppendixD.get(countAppendixD).getAppendixDQuestion());
            }
        });

        buttonSubmitAppendixD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("debug", String.valueOf(countAppendixD));
                hashMapAppendixD.get(countAppendixD).setAppendixDResponse(spinnerAppendixD.getSelectedItem().toString());

                try
                {
                    JSONObject jsonObjectAppendixD = new JSONObject();
                    jsonObjectAppendixD.put("appendixd_q1", hashMapAppendixD.get(1).getAppendixDResponse().toString());
                    jsonObjectAppendixD.put("appendixd_q2", hashMapAppendixD.get(2).getAppendixDResponse().toString());
                    jsonObjectAppendixD.put("appendixd_q3", hashMapAppendixD.get(3).getAppendixDResponse().toString());
                    jsonObjectAppendixD.put("appendixd_q4", hashMapAppendixD.get(4).getAppendixDResponse().toString());
                    jsonObjectAppendixD.put("appendixd_q5", hashMapAppendixD.get(5).getAppendixDResponse().toString());
                    jsonObjectAppendixD.put("appendixd_q6", hashMapAppendixD.get(6).getAppendixDResponse().toString());
                    jsonObjectAppendixD.put("appendixd_q7", hashMapAppendixD.get(7).getAppendixDResponse().toString());
                    jsonObjectAppendixD.put("appendixd_q8", hashMapAppendixD.get(8).getAppendixDResponse().toString());
                    jsonObjectAppendixD.put("appendixd_q9", hashMapAppendixD.get(9).getAppendixDResponse().toString());
                    jsonObjectAppendixD.put("appendixd_q10", hashMapAppendixD.get(10).getAppendixDResponse().toString());
                    jsonObjectAppendixD.put("appendixd_q11", hashMapAppendixD.get(11).getAppendixDResponse().toString());
                    jsonObjectAppendixD.put("appendixd_q12", hashMapAppendixD.get(12).getAppendixDResponse().toString());
                    jsonObjectAppendixD.put("appendixd_q13", hashMapAppendixD.get(13).getAppendixDResponse().toString());
                    jsonObjectAppendixD.put("appendixd_q14", hashMapAppendixD.get(14).getAppendixDResponse().toString());
                    jsonObjectAppendixD.put("appendixd_q15", hashMapAppendixD.get(15).getAppendixDResponse().toString());
                    jsonObjectAppendixD.put("appendixd_q16", hashMapAppendixD.get(16).getAppendixDResponse().toString());
                    jsonObjectAppendixD.put("appendixd_q17", hashMapAppendixD.get(17).getAppendixDResponse().toString());
                    jsonObjectAppendixD.put("appendixd_q18", hashMapAppendixD.get(18).getAppendixDResponse().toString());
                    jsonObjectAppendixD.put("appendixd_q19", hashMapAppendixD.get(19).getAppendixDResponse().toString());
                    jsonObjectAppendixD.put("appendixd_q20", hashMapAppendixD.get(20).getAppendixDResponse().toString());


                    Log.d("debug", jsonObjectAppendixD.toString());
                    if(isConnectedOnline())
                    {
                        new sendAppendixDJsonDataToAWSLinux(jsonObjectAppendixD).execute("http://ec2-52-90-253-42.compute-1.amazonaws.com/appendix-d.php");
                    }
                    else
                    {
                        Toast.makeText(AppendixD_PANAS_Activity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }

                }catch(JSONException e)
                {
                    e.printStackTrace();
                }


            }
        });
    }

    public class sendAppendixDJsonDataToAWSLinux extends AsyncTask<String, Void, String>
    {
        JSONObject jsonObjectAppendixD;

        public sendAppendixDJsonDataToAWSLinux(JSONObject jsonObjectAppendixD)
        {
            this.jsonObjectAppendixD = jsonObjectAppendixD;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialogAppendixD = new ProgressDialog(AppendixD_PANAS_Activity.this);
            pDialogAppendixD.setCancelable(true);
            pDialogAppendixD.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pDialogAppendixD.setTitle("Loading...");
            pDialogAppendixD.setMessage("Saving in Database");
            pDialogAppendixD.show();
        }

        @Override
        protected void onPostExecute(String returnVariable) {
            super.onPostExecute(returnVariable);
            pDialogAppendixD.dismiss();
            Toast.makeText(AppendixD_PANAS_Activity.this, returnVariable, Toast.LENGTH_SHORT).show();
        }

        @Override
        protected String doInBackground(String... params) {
            String returnString = null;
            try
            {
                URL url = new URL(params[0]);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("POST");
                con.setDoOutput(true);
                con.setDoInput(true);
                con.setRequestProperty("Content-Type", "application/json");
                con.connect();

                OutputStreamWriter out = new OutputStreamWriter(con.getOutputStream());
                out.write(jsonObjectAppendixD.toString());
                out.close();

                //Log.d("debug", jsonObjectAppendixB.toString());


                BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
                returnString = br.readLine().toString();
                Log.d("debug", returnString);
                con.disconnect();
                br.close();


            }
            catch (IOException e)
            {
                e.printStackTrace();
            }

            return returnString;
        }
    }


    private boolean isConnectedOnline(){
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if(networkInfo != null && networkInfo.isConnected()){
            return true;
        }
        return false;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
